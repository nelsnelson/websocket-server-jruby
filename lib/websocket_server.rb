#! /usr/bin/env jruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'

require_relative 'logging'
require_relative 'websocket/arguments_parser'
require_relative 'websocket/server'
require_relative 'websocket/telnet_proxy'

# The WebSocket module
module WebSocket
  def echo_server(options)
    ::WebSocket::Server.new(options) do |_ctx, msg|
      format("%<message>s\n", message: msg.upcase)
    end.run
  end

  def telnet_proxy(options)
    handler = ::WebSocket::TelnetProxy.new(options[:telnet_proxy_host], options[:telnet_proxy_port])
    ::WebSocket::Server.new(options, handler).run
  end

  # rubocop: disable Metrics/AbcSize
  def main(args = parse_arguments)
    Logging.log_level = args[:log_level]
    return telnet_proxy(args) unless args[:telnet_proxy_host].nil?
    echo_server(args)
  rescue Interrupt => e
    warn format("\r%<class>s", class: e.class)
    exit
  rescue StandardError => e
    ::WebSocket::Server.log.fatal(e.message)
    e.backtrace.each { |t| WebSocket::Server.log.debug t }
    abort
  end
  # rubocop: enable Metrics/AbcSize
end
# module WebSocket

Object.new.extend(WebSocket).main if $PROGRAM_NAME == __FILE__
