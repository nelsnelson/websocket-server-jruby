# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

# The WebSocket module
module WebSocket
  # The FileServerChannelProgressiveFutureListener class implements
  # handler methods invoked within the server pipeline during a file
  # transfer over a channel.
  class FileServerChannelProgressiveFutureListener
    def operationProgressed(future, progress, total)
      if total.positive?
        log.info "#{future.channel} Transfer progress: #{progress} / #{total}"
      else # total unknown
        log.info "#{future.channel} Transfer progress: #{progress}"
      end
    end

    def operationComplete(future)
      log.debug "#{future.channel} Transfer complete"
    end
  end
end
# module WebSocket
