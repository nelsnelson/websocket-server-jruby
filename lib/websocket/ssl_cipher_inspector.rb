# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

# The WebSocket module
module WebSocket
  java_import Java::io.netty.channel.SimpleChannelInboundHandler
  java_import Java::io.netty.handler.ssl.SslHandler

  # The SslCipherInspector class enables debugging the details around
  # cipher configuration and pipeline handling of SSL handshakes.
  class SslCipherInspector < SimpleChannelInboundHandler
    def initialize
      # Include additional subclass initialization here.
      super()
    end

    def channelRead(ctx, msg)
      cipher_suite = cipher_suite ctx
      log.info "Server communications are secured by #{cipher_suite}"
      ctx.fireChannelRead(msg)
    end

    private

    def cipher_suite(ctx)
      # Confirm the SSL context has been loaded into the channel pipeline
      ssl_handler = ctx.pipeline.get(SslHandler.java_class)
      ssl_engine = ssl_handler.engine()
      ssl_session = ssl_engine.getSession()
      ssl_session.getCipherSuite()
    end
  end
end
