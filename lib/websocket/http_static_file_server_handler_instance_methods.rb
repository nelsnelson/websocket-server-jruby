# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'cgi'
require 'date'

require 'java'
require 'netty'

require_relative 'file_server_channel_progressive_future_listener'

# The WebSocket module
module WebSocket
  java_import Java::io.netty.channel.ChannelFutureListener
  java_import Java::io.netty.channel.DefaultFileRegion
  java_import Java::io.netty.handler.codec.http.DefaultHttpResponse
  java_import Java::io.netty.handler.codec.http.HttpChunkedInput
  java_import Java::io.netty.handler.codec.http.HttpResponseStatus
  java_import Java::io.netty.handler.codec.http.HttpHeaderNames
  java_import Java::io.netty.handler.codec.http.HttpMethod
  java_import Java::io.netty.handler.codec.http.HttpVersion
  java_import Java::io.netty.handler.codec.http.HttpUtil
  java_import Java::io.netty.handler.codec.http.LastHttpContent
  java_import Java::io.netty.handler.ssl.SslHandler
  java_import Java::io.netty.handler.stream.ChunkedFile
  java_import java.io.RandomAccessFile

  # The HttpStaticFileServerHandlerInstanceMethods module
  module HttpStaticFileServerHandlerInstanceMethods
    WEB_SOCKET_PATH_PATTERN_TEMPLATE = '^%<path>s$'.freeze
    FORWARD_SLASH_BEFORE_EOL_PATTERN = %r{/$}
    URI_FORWARD_SLASH_TEMPLATE = '%<uri>s/'.freeze

    # rubocop: disable Metrics/AbcSize
    # rubocop: disable Metrics/CyclomaticComplexity
    # rubocop: disable Metrics/MethodLength
    # rubocop: disable Metrics/PerceivedComplexity
    def messageReceived(ctx, request)
      return if web_socket_path_pattern.match?(request.uri)

      unless request.decoderResult().isSuccess()
        send_error(ctx, HttpResponseStatus::BAD_REQUEST)
        return
      end

      unless request.method() == HttpMethod::GET
        send_error(ctx, HttpResponseStatus::METHOD_NOT_ALLOWED)
        return
      end

      uri = request.uri
      path = sanitize_uri(uri)
      if path.nil?
        send_error(ctx, HttpResponseStatus::FORBIDDEN)
        return
      end

      unless File.exist? path
        send_error(ctx, HttpResponseStatus::NOT_FOUND)
        return
      end

      if File.directory? path
        if FORWARD_SLASH_BEFORE_EOL_PATTERN.match?(uri)
          send_listing(ctx, path)
        else
          send_redirect(ctx, format(URI_FORWARD_SLASH_TEMPLATE, uri: uri))
        end
        return
      end

      unless File.exist? path
        send_error(ctx, HttpResponseStatus::FORBIDDEN)
        return
      end

      # Cache Validation
      modified_since = request.headers().get(HttpHeaderNames::IF_MODIFIED_SINCE)
      if !modified_since.nil? && !modified_since.empty?
        file_last_modified = File.mtime(path).to_s
        # Only compare up to the second because the format of the timestamp
        # sent to the client does not include milliseconds
        modified_since_seconds = DateTime.parse(modified_since).to_time.to_i
        file_last_modified_seconds = DateTime.parse(file_last_modified).to_time.to_i

        if modified_since_seconds == file_last_modified_seconds
          send_not_modified(ctx, file_last_modified_seconds)
          return
        end
      end

      raf = nil
      begin
        raf = RandomAccessFile.new(path, 'r')
      rescue StandardError => _e
        send_error(ctx, HttpResponseStatus::NOT_FOUND)
        return
      end
      file_length = raf.length

      response = DefaultHttpResponse.new(HttpVersion::HTTP_1_1, HttpResponseStatus::OK)
      HttpUtil.setContentLength(response, file_length)
      content_type_header(response, guess_content_type(path))
      date_and_cache_headers(response, path)
      keep_alive_header(response) if HttpUtil.isKeepAlive(request)

      # Write the initial line and the header.
      ctx.write(response)

      send_file_future = nil
      last_content_future = nil
      progressive_promise = ctx.newProgressivePromise()

      # Write the content.
      if ctx.pipeline().get(SslHandler.java_class)
        # SSL enabled - cannot use zero-copy file transfer.
        chunked_file = ChunkedFile.new(raf, 0, file_length, 8192)
        chunked_input = HttpChunkedInput.new(chunked_file)
        send_file_future = ctx.writeAndFlush(chunked_input, progressive_promise)
        # HttpChunkedInput will write the end marker (LastHttpContent) for us.
        last_content_future = send_file_future
      else
        # SSL not enabled - can use zero-copy file transfer.
        file_region = DefaultFileRegion.new(raf.channel, 0, file_length)
        send_file_future = ctx.write(file_region, progressive_promise)
        # Write the end marker.
        last_content_future = ctx.writeAndFlush(LastHttpContent::EMPTY_LAST_CONTENT)
      end

      send_file_future.addListener(FileServerChannelProgressiveFutureListener.new)

      # Decide whether to close the connection or not.
      return if HttpUtil.isKeepAlive(request)

      # Close the connection when the whole content is written out.
      last_content_future.addListener(ChannelFutureListener::CLOSE)
    end
    # rubocop: enable Metrics/AbcSize
    # rubocop: enable Metrics/CyclomaticComplexity
    # rubocop: enable Metrics/MethodLength
    # rubocop: enable Metrics/PerceivedComplexity

    def exceptionCaught(ctx, cause)
      cause.printStackTrace()
      return unless ctx.channel().isActive()
      send_error(ctx, HttpResponseStatus::INTERNAL_SERVER_ERROR)
    end

    private

    def web_socket_path_pattern
      @web_socket_path_pattern ||= begin
        pattern = format(WEB_SOCKET_PATH_PATTERN_TEMPLATE, path: web_socket_path)
        Regexp.new(pattern)
      end
    end

    def web_socket_path
      @web_socket_path ||= begin
        value = options[:web_socket_path]
        value.nil? || value.empty? ? DEFAULT_WEB_SOCKET_PATH : value
      end
    end
  end
  # module HttpStaticFileServerHandlerInstanceMethods
end
# module WebSocket
