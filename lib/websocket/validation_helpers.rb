# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require_relative 'encoding'

# The WebSocket module
module WebSocket
  # The ValidationHelpers module
  module ValidationHelpers
    FILE_SEPARATOR_DOT_PATTERN = %r{#{File::SEPARATOR}\.}
    DOT_FILE_SEPARATOR_PATTERN = %r{\.#{File::SEPARATOR}}
    QUERY_STRING_PATTERN = %r{\?.*}
    FORWARD_SLASH_PATTERN = %r{/}

    # Simplistic dumb security check.
    # Something more serious is required in a production environment.
    def insecure_uri?(uri)
      FILE_SEPARATOR_DOT_PATTERN.match?(uri) ||
        DOT_FILE_SEPARATOR_PATTERN.match?(uri) ||
        uri.start_with?('.') ||
        uri.end_with?('.') ||
        insecure_uri_pattern.match?(uri)
    end

    def sanitize_uri(uri)
      # Decode the path.
      uri = CGI.unescape(uri.gsub(QUERY_STRING_PATTERN, ''), WebSocket::Encoding.name)
      return nil if uri.empty? || !uri.start_with?('/')
      # Convert file separators.
      uri = uri.gsub(FORWARD_SLASH_PATTERN, File::SEPARATOR)
      return nil if insecure_uri?(uri)
      # Convert to absolute path.
      File.join(web_root, uri)
    end

    private

    def web_root
      @web_root ||= begin
        value = options[:web_root]
        value || DEFAULT_WEB_ROOT
      end
    end

    def insecure_uri_pattern
      @insecure_uri_pattern ||= begin
        value = options[:insecure_uri_pattern]
        value || DEFAULT_INSECURE_URI_PATTERN
      end
    end
  end
  # module ValidationHelpers
end
# module WebSocket
