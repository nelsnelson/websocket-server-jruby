# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

require_relative 'frame_handler'

# The WebSocket module
module WebSocket
  java_import Java::io.netty.channel.ChannelHandler
  java_import Java::io.netty.channel.ChannelFutureListener
  java_import Java::io.netty.handler.codec.http.websocketx.TextWebSocketFrame

  # The MessageHandler class provides a method for specifying code
  # to handle incoming messages and respond with the results from
  # the given code.
  class MessageHandler < WebSocket::FrameHandler
    include ChannelHandler
    include ChannelFutureListener

    def initialize(&handler)
      super()
      @handler = handler
    end

    def handle_message(ctx, message)
      request = message&.to_s&.strip
      return if request.nil?
      response = @handler.call(ctx, request)
      return if response.nil?
      log.debug "#{self.class}#handle_message response: #{response.chomp}"
      ctx.channel.writeAndFlush(TextWebSocketFrame.new(response))
      response
    end
  end
end
# module WebSocket
