# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

require_relative 'http_static_file_server_handler_instance_methods'
require_relative 'header_helpers'
require_relative 'response_helpers'
require_relative 'validation_helpers'

# The WebSocket module
module WebSocket
  java_import Java::io.netty.channel.SimpleChannelInboundHandler
  java_import Java::io.netty.handler.codec.http.FullHttpRequest

  # The HttpStaticFileServerHandler class supports classical
  # file server semantics.
  class HttpStaticFileServerHandler < SimpleChannelInboundHandler
    include WebSocket::HttpStaticFileServerHandlerInstanceMethods
    include WebSocket::HeaderHelpers
    include WebSocket::ResponseHelpers
    include WebSocket::ValidationHelpers
    attr_reader :options

    def initialize(options = nil)
      super(FullHttpRequest.java_class)
      @options = options
    end

    # Please keep in mind that this method will be renamed to
    # messageReceived(ChannelHandlerContext, I) in 5.0.
    #
    # protected abstract void channelRead0(ChannelHandlerContext ctx, I msg) throws Exception
    def channelRead0(ctx, message)
      messageReceived(ctx, message)
    end
  end
  # class HttpStaticFileServerHandler
end
# module WebSocket
