# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

require_relative 'idle_state_user_event_handler'

# The WebSocket module
module WebSocket
  java_import Java::io.netty.handler.codec.http.websocketx.TextWebSocketFrame

  # The IdleHandler class handles idle channels detected by the
  # server pipeline.
  class IdleHandler < WebSocket::IdleStateUserEventHandler
    def initialize
      # Include additional subclass initialization here.
      super()
    end

    def handle_idle_channel(ctx, _evt)
      klass = self.class.name
      log.debug "#{klass} < IdleStateUserEventHandler ##{__method__}"
      message = TextWebSocketFrame.new("\nDisconnecting idle session\n")
      # TODO: Test
      ctx.writeAndFlush(message).sync
      ctx.close
      # ctx.channel.writeAndFlush(message).sync
      # ctx.channel.disconnect().awaitUninterruptibly()
      # ctx.channel.close().awaitUninterruptibly()
    end
  end
end
