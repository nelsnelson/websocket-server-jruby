# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

# The WebSocket module
module WebSocket
  java_import Java::io.netty.channel.ChannelFutureListener
  java_import Java::io.netty.channel.ChannelHandler
  java_import Java::io.netty.handler.codec.http.FullHttpRequest
  java_import Java::io.netty.handler.codec.http.websocketx.WebSocketFrame
  java_import Java::io.netty.handler.codec.http.websocketx.TextWebSocketFrame

  # The FrameHandler class implements a handler for incoming
  # WebSocket request messages.  This handler invokes a #handle_message
  # method which should be implemented by a user provided subclass.  The
  # MessageHandler class is an example of one such subclass.
  class FrameHandler < SimpleChannelInboundHandler
    include ChannelHandler
    include ChannelFutureListener
    UNSUPPORTED_FRAME_TYPE_ERROR_TEMPLATE =
      '%<handler>s encountered unsupported frame type: %<frame>s'.freeze

    def initialize
      super(WebSocketFrame.java_class)
    end

    # Please keep in mind that this method will be renamed to
    # messageReceived(ChannelHandlerContext, I) in 5.0.
    #
    # java_signature 'protected void channelRead0(ChannelHandlerContext ctx, WebSocketFrame frame) throws Exception'
    def channelRead0(ctx, frame)
      messageReceived(ctx, frame)
    end

    def handle_message(ctx, msg)
      # Send the uppercase string back.
      ctx.channel.writeAndFlush(TextWebSocketFrame.new(msg.to_s.strip.upcase))
    end

    def unsupported_frame(frame, handler)
      raise java.lang.UnsupportedOperationException, format(
        UNSUPPORTED_FRAME_TYPE_ERROR_TEMPLATE,
        handler: handler.class,
        frame: frame.class
      )
    end

    def messageReceived(ctx, frame)
      # ping and pong frames already handled
      case frame
      when TextWebSocketFrame
        unsupported_frame(frame, self) unless frame.respond_to?(:text)
        handle_message(ctx, frame.text)
      when FullHttpRequest
        # Let another handler handle it.
      else unsupported_frame(frame, self)
      end
    end
  end
  # class WebSocketFrameHandler
end
# module WebSocket
