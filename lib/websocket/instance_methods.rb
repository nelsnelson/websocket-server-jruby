# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

require_relative 'channel_initializer'
require_relative 'shutdown_hook'

# The WebSocket module
module WebSocket
  java_import Java::io.netty.bootstrap.ServerBootstrap
  java_import Java::io.netty.channel.ChannelOption

  # The InstanceMethods module
  module InstanceMethods
    def channel_initializer
      @channel_initializer ||= ::WebSocket::ChannelInitializer.new(channel_group, @options)
    end

    # rubocop: disable Metrics/AbcSize
    # rubocop: disable Metrics/MethodLength
    def run(params = {})
      @options.merge!(params)
      channel = bootstrap.bind(port).sync().channel()
      channel_group.add(channel)
      ::WebSocket::ShutdownHook.new(self)
      log.info "Listening on #{channel.local_address}"
      channel.closeFuture().sync()
    rescue java.net.BindException => e
      raise "Bind error: #{e.message}: #{@options[:host]}:#{port}"
    rescue java.net.SocketException => e
      raise "Socket error: #{e.message}: #{@options[:host]}:#{port}"
    ensure
      stop
    end
    # rubocop: enable Metrics/AbcSize
    # rubocop: enable Metrics/MethodLength

    def port
      @port ||= (@options[:ssl] ? @options[:ssl_port] : @options[:port]).to_i
    end

    def log_requests?
      @log_requests ||= begin
        value = @options[:log_requests]
        value.nil? ? DEFAULT_LOG_REQUESTS : value
      end
    end
  end
  # module ServerInstanceMethods
end
# module WebSocket
