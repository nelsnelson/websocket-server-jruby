# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

# The WebSocket module
module WebSocket
  java_import Java::io.netty.channel.ChannelDuplexHandler
  java_import Java::io.netty.handler.codec.http.websocketx.PingWebSocketFrame
  java_import Java::io.netty.handler.timeout.IdleState

  # The PingMessage class is just a convenient alias
  # for the PingWebSocketFrame class.
  PingMessage = Class.new(PingWebSocketFrame)

  # The IdleStateUserEventHandler class specifies methods implementing
  # what to do when the pipeline detects an idle channel.
  class IdleStateUserEventHandler < ChannelDuplexHandler
    def initialize
      super()
    end

    # java_signature 'public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception'
    def userEventTriggered(ctx, evt)
      return unless evt.respond_to?(:state)
      case evt.state
      when IdleState::READER_IDLE
        return handle_idle_channel(ctx, evt) if respond_to?(:handle_idle_channel)
        message = TextWebSocketFrame.new("\nDisconnecting idle session\n")
        ctx.writeAndFlush(message).sync
        ctx.close
      when IdleState::WRITER_IDLE
        ctx.writeAndFlush(PingMessage.new)
      end
    end
  end
end
