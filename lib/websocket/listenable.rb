# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

# The WebSocket module
module WebSocket
  # The Listenable module
  module Listenable
    include ::Server::Listenable
  end
end
