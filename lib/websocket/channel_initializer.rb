# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

require_relative 'http_static_file_server_handler'
require_relative 'idle_handler'
require_relative 'message_handler'

# The WebSocket module
module WebSocket
  java_import Java::io.netty.handler.codec.http.HttpObjectAggregator
  java_import Java::io.netty.handler.codec.http.HttpServerCodec
  java_import Java::io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler
  java_import Java::io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketServerCompressionHandler
  java_import Java::io.netty.handler.stream.ChunkedWriteHandler
  java_import Java::io.netty.handler.timeout.IdleStateHandler

  # The ChannelInitializer class programmatically specifies the
  # configuration for the channel pipeline that will handle requests of
  # the server.
  class ChannelInitializer < ::Server::ChannelInitializer
    HTTP_OBJECT_AGGREGATOR_BUFFER_BYTES_SIZE = 65_536

    def initialize(channel_group, options = {})
      super(channel_group, options)
    end

    # rubocop: disable Metrics/AbcSize
    # rubocop: disable Metrics/MethodLength
    def initChannel(channel)
      pipeline = channel.pipeline
      pipeline.addLast(ssl_handler(channel)) if ssl?
      pipeline.addLast(HttpServerCodec.new)
      pipeline.addLast(HttpObjectAggregator.new(HTTP_OBJECT_AGGREGATOR_BUFFER_BYTES_SIZE))
      pipeline.addLast(ChunkedWriteHandler.new)
      pipeline.addLast(IdleStateHandler.new(idle_reading, idle_writing, 0))
      pipeline.addLast(WebSocket::IdleHandler.new)
      pipeline.addLast(WebSocketServerCompressionHandler.new)
      pipeline.addLast(WebSocketServerProtocolHandler.new(web_socket_path, nil, true))
      pipeline.addLast(SslCipherInspector.new) if !ssl_context.nil? && inspect_ssl?
      pipeline.addLast(HttpStaticFileServerHandler.new(@options))
      add_user_handlers(pipeline)
      pipeline.addLast(default_handler)
    end
    # rubocop: enable Metrics/AbcSize
    # rubocop: enable Metrics/MethodLength

    protected

    def add_user_handlers(pipeline)
      @user_handlers.each do |handler|
        case handler
        when Class then pipeline.addLast(handler.new)
        when Proc then pipeline.addLast(::WebSocket::MessageHandler.new(&handler))
        else pipeline.addLast(handler)
        end
      end
    end

    private

    def web_socket_path
      @web_socket_path ||= begin
        value = @options[:web_socket_path]
        value.nil? || value.empty? ? DEFAULT_WEB_SOCKET_PATH : value
      end
    end

    def idle_reading
      @idle_reading ||= begin
        value = @options[:idle_reading]
        value.nil? ? DEFAULT_IDLE_READING_SECONDS : value
      end
    end

    def idle_writing
      @idle_writing ||= begin
        value = @options[:idle_writing]
        value.nil? ? DEFAULT_IDLE_WRITING_SECONDS : value
      end
    end

    def ssl?
      @ssl ||= begin
        value = @options[:ssl]
        value.nil? ? DEFAULT_SSL_ENABLED : value
      end
    end

    def inspect_ssl?
      @inspect_ssl = begin
        value = @options[:inspect_ssl]
        value.nil? ? DEFAULT_SSL_INSPECTION_ENABLED : value
      end
    end
  end
  # class ServerInitializer
end
# module WebSocket
