# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

# The WebSocket module
module WebSocket
  # The TelnetProxy class adds a handler to the server channel
  # pipeline which will proxy incoming requests to a telnet server.
  class TelnetProxy
    def initialize(host, port)
      pipeline << TelnetProxyFrontendHandler.new(host, port)
    end
  end
end
