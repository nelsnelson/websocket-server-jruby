# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

# The WebSocket module
module WebSocket
  java_import Java::io.netty.util.CharsetUtil
  Encoding = CharsetUtil::UTF_8
  HtmlContentType = "text/html; charset=#{Encoding.name}".freeze
end
