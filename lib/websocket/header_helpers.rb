# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

require_relative '../server/mime_types'

# The WebSocket module
module WebSocket
  java_import Java::io.netty.handler.codec.http.HttpHeaderValues

  # The HeaderHelpers module
  module HeaderHelpers
    PRIVATE_MAX_AGE_TEMPLATE = 'private, max-age=%<max_age>s'.freeze

    def keep_alive_header(response)
      response.headers().set(HttpHeaderNames::CONNECTION, HttpHeaderValues::KEEP_ALIVE)
    end

    def date_header(response, date)
      response.headers().set(HttpHeaderNames::DATE, date)
    end

    def expires_header(response, expires)
      response.headers().set(HttpHeaderNames::EXPIRES, expires)
    end

    def cache_control_header(response, cache_control)
      response.headers().set(HttpHeaderNames::CACHE_CONTROL, cache_control)
    end

    def last_modified_header(response, last_modified)
      response.headers().set(HttpHeaderNames::LAST_MODIFIED, last_modified)
    end

    # rubocop: disable Metrics/AbcSize
    def date_and_cache_headers(response, path, timestamp = Time.now)
      maximum_age = options[:http_cache_seconds]
      date_format = options[:http_date_format]
      date_header(response, timestamp.strftime(date_format))
      expires_header(response, Time.at(timestamp.to_i + maximum_age).strftime(date_format))
      cache_control_header(response, format(PRIVATE_MAX_AGE_TEMPLATE, max_age: maximum_age.to_s))
      last_modified_header(response, File.mtime(path).strftime(date_format))
    end
    # rubocop: enable Metrics/AbcSize

    def content_type_header(response, content_type)
      response.headers().set(HttpHeaderNames::CONTENT_TYPE, content_type)
    end

    def guess_content_type(path, _charset = nil)
      i = path.rindex(/\./)
      return nil if i == -1
      extension = path[(i + 1)..].downcase.to_sym
      ::Server::MimeTypes.fetch(extension, ::Server::MimeTypes[:txt])
    end
  end
  # module HeaderHelpers
end
# module WebSocket
