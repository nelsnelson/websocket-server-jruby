# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

# The WebSocket module
module WebSocket
  # This module implements methods responsible for the
  # initialization of an SSL context
  module SslContextInitializationMethods
    java_import Java::io.netty.handler.ssl.SslContextBuilder
    java_import Java::io.netty.handler.ssl.SslProvider
    java_import Java::io.netty.handler.ssl.util.SelfSignedCertificate
    java_import java.io.FileInputStream
    java_import java.io.BufferedInputStream
    java_import java.io.ByteArrayOutputStream
    java_import java.security.KeyFactory
    java_import java.security.cert.CertificateFactory
    java_import java.security.spec.PKCS8EncodedKeySpec

    DefaultCertificateType = 'X.509'.freeze

    def init_ssl(channel_initializer)
      return unless options[:ssl] && channel_initializer.respond_to?(:ssl_context)
      context = ssl_context
      channel_initializer.ssl_context = context unless context.nil?
    end

    def all_exist?(*files)
      files.all? { |f| File.exist?(f) }
    end

    def init_ssl_context(certificate, private_key)
      return certificate_key_pair(certificate, private_key) if all_exist?(certificate, private_key)
      options[:use_jdk_ssl_provider] ? jdk_ssl_provider : self_signed_certificate
    end

    def ssl_context
      @ssl_context ||= init_ssl_context(options[:ssl_certificate_file_path], options[:ssl_private_key_file_path])
    end

    def certificate_key_pair(certificate, private_key)
      log.info "Securing socket layer using #{certificate} and #{private_key}"
      certificate = read_certificate(certificate)
      private_key = read_private_key(private_key)
      SslContextBuilder.forServer(private_key, certificate).build()
    end

    def jdk_ssl_provider
      log.info 'Securing socket layer using JDK self-signed certificate'
      ssc = SelfSignedCertificate.new
      context_builder = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey())
      context_builder.sslProvider(SslProvider::JDK).build()
    end

    def self_signed_certificate
      log.info 'Securing socket layer using self-signed certificate'
      ssc = SelfSignedCertificate.new
      SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build()
    end

    def read_certificate(file_path)
      file_input_stream = FileInputStream.new(file_path)
      buffered_input_stream = BufferedInputStream.new(file_input_stream)
      CertificateFactory.getInstance(DefaultCertificateType).generateCertificate(buffered_input_stream)
    end

    def read_private_key(file_path)
      file_input_stream = FileInputStream.new(file_path)
      buffered_input_stream = BufferedInputStream.new(file_input_stream)
      decode_private_key(stream_data(buffered_input_stream))
    ensure
      close_streams(buffered_input_stream, file_input_stream)
    end

    def stream_data(input_stream, output_stream = ByteArrayOutputStream.new)
      buffer = Java::byte[1024 * 4].new
      loop do
        n = input_stream.read(buffer)
        break if n == -1
        output_stream.write(buffer, 0, n)
      end
      output_stream.toByteArray()
    ensure
      close_streams(output_stream)
    end

    def close_streams(*streams)
      streams.each { |stream| safely_close_stream(stream) }
    end

    def safely_close_stream(stream)
      stream.close()
    rescue StandardError => e
      log.warn "Failed to close stream: #{e.message}"
    end

    def decode_private_key(encoded_private_key)
      private_key_spec = PKCS8EncodedKeySpec.new(encoded_private_key)
      KeyFactory.getInstance('RSA').generatePrivate(private_key_spec)
    end
  end
  # module SslContextInitializationMethods
end
# module WebSocket
