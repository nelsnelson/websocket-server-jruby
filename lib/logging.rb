# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin

# Copyright Nels Nelson 2016-2019 but freely usable (see license)

# =end

require 'java'
require 'fileutils'
require 'logger'

require 'apache-log4j-2'

# The Logging module
module Logging
  # rubocop: disable Metrics/MethodLength
  def config
    @config ||= begin
      lib_dir_path = File.expand_path(__dir__)
      project_dir_path = File.expand_path(File.dirname(lib_dir_path))
      logs_dir_path = File.expand_path(File.join(project_dir_path, 'logs'))
      FileUtils.mkdir_p(logs_dir_path)
      log_file_path = File.expand_path(File.join(logs_dir_path, 'server.log'))
      FileUtils.touch(log_file_path)
      {
        level: Logger::INFO,
        name: 'websocket',
        lib_dir_path: lib_dir_path,
        project_dir_path: project_dir_path,
        logs_dir_path: logs_dir_path,
        log_file_path: log_file_path,
        rolling_log_file_name_template: File.expand_path(
          File.join(logs_dir_path, 'server-%d{yyyy-MM-dd}.log.gz')),
        logging_pattern_template: {
          java: '%d{ABSOLUTE} %-5p [%c{1}] %m%n',
          ruby: "%<timestamp>s %-5<severity>s [%<progname>s] %<msg>s\n"
        },
        schedule: '0 0 0 * * ?',
        size: '100M'
      }
    end
  end
  module_function :config
  # rubocop: enable Metrics/MethodLength
end

# The Logging module
module Logging
  if defined?(Java)
  java_import Java::org.apache.logging.log4j.core.appender::ConsoleAppender
  java_import Java::org.apache.logging.log4j.core.config::Configurator
  java_import Java::org.apache.logging.log4j.core.config.builder.api::ConfigurationBuilderFactory
  end

  # rubocop: disable Metrics/AbcSize
  # rubocop: disable Metrics/MethodLength
  def init_log4j(log_level = org.apache.logging.log4j.Level::INFO)
    java.lang::System.setProperty('log4j.shutdownHookEnabled', java.lang::Boolean.toString(false))
    config = ConfigurationBuilderFactory.newConfigurationBuilder()

    log_level = org.apache.logging.log4j::Level.to_level(log_level.to_s.upcase) if log_level.is_a? Symbol
    config.setStatusLevel(log_level)
    config.setConfigurationName('websocket')

    # create a console appender
    target = ConsoleAppender::Target::SYSTEM_OUT
    pattern = Logging.config[:logging_pattern_template][:java]
    layout = config.newLayout('PatternLayout')
    layout = layout.addAttribute('pattern', pattern)
    appender = config.newAppender('stdout', 'CONSOLE')
    appender = appender.addAttribute('target', target)
    appender = appender.add(layout)
    config.add(appender)

    # create a root logger
    root_logger = config.newRootLogger(log_level)
    root_logger = root_logger.add(config.newAppenderRef('stdout'))

    # create a rolling file appender
    cron = config.newComponent('CronTriggeringPolicy')
    cron = cron.addAttribute('schedule', '0 0 0 * * ?')

    size = config.newComponent('SizeBasedTriggeringPolicy')
    size = size.addAttribute('size', '100M')

    policies = config.newComponent('Policies')
    policies = policies.addComponent(cron)
    policies = policies.addComponent(size)

    appender = config.newAppender('rolling_file', 'RollingFile')
    appender = appender.addAttribute('fileName', Logging.config[:log_file_path])
    appender = appender.addAttribute('filePattern', Logging.config[:rolling_log_file_name_template])
    appender = appender.add(layout)
    appender = appender.addComponent(policies)
    config.add(appender)

    root_logger = root_logger.addAttribute('additivity', false)
    root_logger = root_logger.add(config.newAppenderRef('rolling_file'))
    config.add(root_logger)

    logging_configuration = config.build()
    ctx = Configurator.initialize(logging_configuration)
    ctx.updateLoggers()
  end
  module_function :init_log4j
  # rubocop: enable Metrics/AbcSize
  # rubocop: enable Metrics/MethodLength
  # def init_log4j

  init_log4j if defined?(Java)
end
# module Logging

# Namespace for methods to help with implicit backtrace printing
module LoggerHelpers
  def generate_message(error_or_message, error)
    error_message = "#{error_or_message}: #{error.class.name}"
    error_message << ": #{error.message}" if error.respond_to?(:message)
    error_message
  end

  def extract_backtrace(error, default_result = nil)
    if error.respond_to?(:backtrace)
      error.backtrace.each { |trace| original_error(trace) unless trace.nil? }
    elsif error.respond_to?(:getStackTrace)
      error.getStackTrace().each { |trace| original_error(trace) unless trace.nil? }
    else
      default_result
    end
  end
end

# Monkey-patch the built-in Ruby Logger class to support
# implicit backtrace printing
# TODO: Figure out if this is actually useful.
class Logger
  include LoggerHelpers

  alias original_error error
  def error(error_or_message, error = nil)
    return extract_backtrace(error_or_message) if error.nil?
    original_error(generate_message(error_or_message, error))
    extract_backtrace(original_error(error))
  end
end

# The Logging module
module Logging
  if defined?(Java)
  java_import Java::org.apache.logging.log4j.Level
  java_import Java::org.apache.logging.log4j.LogManager
  end

  FORWARD_SLASH_PATTERN = %r{/} unless defined? FORWARD_SLASH_PATTERN

  def init_logger(level = :info, logger_name = nil)
    return init_java_logger(level, logger_name, caller[2]) if defined?(Java)
    init_ruby_logger(level, logger_name, caller[2])
  end

  def ruby_log_formatter(severity_level, datetime, program_name, message)
    format(
      Logging.config[:logging_pattern_template][:ruby],
      timestamp: datetime.strftime(Logging.config[:logging_timestamp_format]),
      progname: program_name, severity: severity_level, msg: message)
  end

  def init_ruby_logger(level = nil, logger_name = nil, source_location = nil)
    logger_name = get_formatted_logger_name(logger_name)
    logger_name = source_location.split(FORWARD_SLASH_PATTERN).last if logger_name.empty?
    log = Logger.new($stdout, progname: logger_name)
    log.level = level.to_s unless level.nil?
    log.formatter = method(:ruby_log_formatter)
    log
  end

  def init_java_logger(level = nil, logger_name = nil, source_location = nil)
    logger_name = get_formatted_logger_name(logger_name)
    logger_name = source_location.split(FORWARD_SLASH_PATTERN).last if logger_name.empty?
    log = LogManager.getLogger(logger_name)
    log.level = Level.to_level(level.to_s.upcase) unless level.nil?
    log
  end

  def get_formatted_logger_name(logger_name = nil)
    return logger_name.to_s[/\w+$/] unless logger_name.nil?
    return name[/\w+$/] if is_a?(Class) || is_a?(Module)
    self.class.name[/\w+$/]
  end

  # rubocop: disable Metrics/CyclomaticComplexity
  # OFF: 0
  # FATAL: 100
  # ERROR: 200
  # WARN: 300
  # INFO: 400
  # DEBUG: 500
  # TRACE: 600
  # ALL: 2147483647
  # See: https://logging.apache.org/log4j/2.x/log4j-api/apidocs/org/apache/logging/log4j/Level.html
  def symbolize_numeric_log_level(level)
    case level
    when 5..Float::INFINITY then :off
    when 4 then :fatal
    when 3 then :error
    when 2 then :warn
    when 1 then :info
    when 0 then :debug
    when -1 then :trace
    when -Float::INFINITY..-2 then :all
    end
  end
  # rubocop: enable Metrics/CyclomaticComplexity

  def log_level=(level)
    Logging.config[:level] = symbolize_numeric_log_level(level)
  end
  module_function :log_level=

  def log_level
    Logging.config[:level]
  end
  module_function :log_level

  def log(level = Logging.log_level, log_name = Logging.config[:app_name])
    @log ||= init_logger(level, log_name)
  end
  alias logger log
end
# module Logging

# The Module class
class Module
  # Universally include Logging
  include ::Logging
end

# The Class class
class Class
  # Universally include Logging
  include ::Logging
end

# The Object class
class Object
  # Universally include Logging
  include ::Logging
end
