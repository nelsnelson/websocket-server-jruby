# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'rubygems'
require 'geminabox'

Geminabox.data = '.geminabox/data'
Geminabox.rubygems_proxy = true
run Geminabox::Server
