# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

RSpec.configure do |config|
  config.success_color = :cyan
  config.after(:suite) do
    puts 'Tests complete'
  end
end
