
var mobile = navigator.userAgent.match(/(iPhone|iPad|Android)/i);
var iphone = navigator.userAgent.match(/iPhone/i);
var old_doc = 0;

function input(el) {
    _scroll();
    if (client != null && !client.isConnected()) {
        resetConnection();
        el.value = '';
        return false;
    }

    var record = true;
    var message = '';
    var label = $('label.' + el.id);
    if (el.id == 'password') {
        _write(label.html().toString() + el.value.replace(/./g, '&bull;'));
        record = false
        message = el.value;
        $(el).hide();
        $(label).hide();
        focus($('input#input'));
    } else {
        _write(label.html().toString() + el.value);
        message = el.value.replace(/[\x00-\x1F\x7F-\x9F]/g, '');
    }

    el.value = '';
    label.html('');
    client.send(message, record);
    return false;
}

function send_message(message, record) {
    client.send(message, record);
}

function receive_message(message) {
    message = message ? message : '';
    var text = $.trim(message);
    if (text.length == 0) return;
    if (text.match(/The current time is now /)) game_started();

    var lines = message.split(/\r?\n/);
    var previous_prompt = ($('form.input label:visible').html() || '').toString();
    if (previous_prompt.length > 0) lines.unshift(previous_prompt);
    var prompt = previous_prompt.replace(/&nbsp;/, ' ');
    var new_prompt = (lines.pop() || '').toString();
    if (prompt.length == 0) prompt = new_prompt;
    message = lines.join("\n");

    if (prompt.match(/password\:\s?$/)) {
        hide_input_field();
        show_password_field();
    } else if (!$('input#input').is(':visible')) {
        hide_password_field();
        show_input_field();
    } else {
        show_input_field();
    }

    if (!(typeof showdown === 'undefined') && message.match(/---\npath: .*\.md/m)) {
        message = handle_markdown(message);
    } else {
        message = message.replace(/\>/g, '___:').replace(/\</g, ':___');
        message = ansispan(message);
        message = message.replace(/^[\x00-\x0B][\x0C][\x0E-\x1F]/g, '');
        message = message.replace(/  /g, "&nbsp;&nbsp;");
        message = message.replace(/ $/g, "&nbsp;");
        message = message.replace(/^[\n\x0D]+/, "\n");
        log(message.replace(/^[\x00-\x1F]+/, ''));
        message = message.replace(/[\x00-\x0B][\x0C][\x0E-\x1F]/g, '');
        message = message.replace(/\>/g, '&gt;').replace(/\</g, '&lt;');
        message = buffer.html(message).text();
        message = message.replace(/___:/g, '&gt;').replace(/:___/g, '&lt;');
        message = hyperlinkdirections(message);
        message = hyperlinkurls(message);
    }
    if (message.length > 0) println(message);
    print(prompt);
}

function show_input_field() {
    var input       = $('input#input');
    var input_label = $('label.input');
    input.css('visibility', 'visible');
    input.show();
    input.removeClass('hidden');
    input_label.css('visibility', 'visible');
    input_label.show();
    input_label.removeClass('hidden');
    focus(input);
}

function hide_input_field() {
    var input       = $('input#input');
    var input_label = $('label.input');
    input.css('visibility', 'hidden');
    input.hide();
    input.addClass('hidden');
    input_label.css('visibility', 'hidden');
    input_label.hide();
    input_label.addClass('hidden');
}

function show_password_field() {
    var password = $('input#password');
    var password_label = $('label.password');
    password.css('visibility', 'visible');
    password.show();
    password.removeClass('hidden');
    password_label.css('visibility', 'visible');
    password_label.show();
    password_label.removeClass('hidden');
    focus(password);
}

function hide_password_field() {
    var password = $('input#password');
    var password_label = $('label.password');
    password.css('visibility', 'hidden');
    password.hide();
    password.addClass('hidden');
    password_label.css('visibility', 'hidden');
    password_label.addClass('hidden');
    password_label.hide();
}

function handle_markdown(s) {
    s = s.replace(/---\n.*\n---\n/m, '');
    var prompt = s.split(/\r?\n/).pop();
    s = s.substring(0, s.lastIndexOf(prompt));
    s = new showdown.Converter().makeHtml(s);
    return "<div class=\"markdown\">\n" + s + "</div>\n" + prompt;
}

function previous_command(el) {
    if (client.index > 0) {
        if (client.index == client.history.length - 1) {
            client.history.push(el.value);
            client.index++;
        }
        client.index--;
        el.value = client.history[client.index];
    }
    select_input(el);
}

function next_command(el) {
    if (client.index < (client.history.length - 1)) {
        client.index++;
        el.value = client.history[client.index];
        if (client.index == client.history.length - 1) {
            client.history.pop();
            client.index--;
        }
    }
    select_input(el);
}

var buffer = $("<div/>");

function _write(message, display) {
    message = message.replace(/\>/g, '&gt;').replace(/\</g, '&lt;');
    println(message, display);
}

function print(message, display) {
    message = message ? message : '';
    display = display ? display : $('div.display');
    var input = display.find('div.input');
    var form = display.find('form.input');
    var type = message.match(/^.*password:\s?/) ? 'password' : 'input';
    var label = form.find('label.' + type);
    label.html(message.replace(/\s$/, "&nbsp;"));
    // Impose a limit of elements displayed as output history in
    // order to restrain lag
    while (display.children().length > 200) {
        var div = document.getElementsByClassName('display')[0]
        div.removeChild(div.firstChild);
    }
    _scroll();
}

function println(message) {
    message = message ? message : '';
    var display = $('div.display');
    var fragment = document.createDocumentFragment();
    var span = document.createElement('span');
    var br = document.createElement('br');
    span.setAttribute('class', 'user');
    span.innerHTML = message;
    fragment.appendChild(span);
    fragment.appendChild(br);
    var input = display.find('div.input');
    input.before(fragment.cloneNode(true));
    // Impose a limit of elements displayed as output history in
    // order to restrain lag
    while (display.children().length > 200) {
        var div = document.getElementsByClassName('display')[0]
        div.removeChild(div.firstChild);
    }
    _scroll();
}

function _scroll() {
    $('html').scrollTop(1E10);
    var display = $('div.display');
    display.scrollTop = display.scrollHeight;
}

function log(message) {
    if (window.location.hostname == 'localhost') {
        console.log(message);
    }
}

function reset(display) {
    display = display ? display : $('div.display');
    var input = display.find('div.input');
    $('div.line').html(input);
    display.find('input#input, input#password').val('');
}

function clear(display) {
    display = display ? display : $('div.display');
    var input = display.find('input#input');
    var password = display.find('input#password');
    var line = display.find('div.line');
    display.html(display.find('div.input'));
    display.find('input#input').val(input.val());
    display.find('input#password').val(password.val());
    display.find('input#input, input#password').keydown(input_keydown);
}

function focus(field) {
    field = field ? field : $('input#input');
    field.focus();
    field.select();
    select_input(field);
    // if (iphone) $('div.display').click();
}

/*
function getSelectionText() {
    var text = '';
    if (window.getSelection) {
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
        text = document.selection.createRange().text;
    }
    return text;
}
*/

function text_selection() {
    var text = '';
    if (typeof window.getSelection != 'undefined') {
        text = window.getSelection().toString();
    } else if (typeof document.selection != 'undefined' && document.selection.type == 'Text') {
        text = document.selection.createRange().text;
    }
    return text;
}

function text_selected() {
    var selected = false;
    if (typeof window.getSelection != 'undefined') {
        selected = !(window.getSelection().isCollapsed);
    } else if (typeof document.getSelection != 'undefined') {
        selected = !(window.getSelection().isCollapsed);
    } else if (typeof document.selection != 'undefined' && document.selection.type == 'Text') {
        // selected = (document.selection.createRange().text.length > 0);
        selected = (document.selection.createRange().boundingWidth > 0);
    }
    return selected;
}

function select_input(e) {
    var el = e && e.target ? e.target : e;
    if (text_selected()) {
        return;
    }
    var input    = $('input#input');
    var password = $('input#password');
    var field = null;
    if (input.is(':visible')) {
        field = input;
    } else if (password.is(':visible')) {
        field = password;
    }
    if (!field) {
        return;
    }
    field.focus();
    var l = field.value ? field.value.length : 0;
    if (field.createTextRange) {
        var textRange = field.createTextRange();
        textRange.collapse(true);
        textRange.moveEnd(l);
        textRange.moveStart(l);
        textRange.select();
    } else if (field.setSelectionRange) {
        field.setSelectionRange(l, l);
    }
}

var directions = [
    'north', 'south', 'east', 'west',
    'northeast', 'southeast', 'northwest', 'southwest',
    'up', 'down'
].join('|');
var compass_direction = new RegExp("\>(" + directions + ")\<\/span\>", 'g');
var direction_link = "><a class=\"direction\">$1</a></span>";
var hyperlinkdirections = function(str) {
    return str.replace(compass_direction, direction_link);
};
var go = function(e) {
    var prompt_label = $('form.input label:visible');
    if (prompt_label.hasClass('password')) return;
    var current_prompt = prompt_label.html().toString();
    if (current_prompt.match(/[:\?](&nbsp;|\s)?$/)) return;
    var el = e ? $(e.target) : $(this);
    var dir = el.text();
    var field = $('form.input input:visible');
    field.val(dir);
    input(field.get(0));
};
var hyperlinkurls = function(str) {
    return str.linkify();
};
if(!String.linkify) {
    String.prototype.linkify = function() {

        // http://, https://, ftp://
        var urlPattern = /\b(?:https?|ftp):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|]/gim;

        // www. sans http:// or https://
        var pseudoUrlPattern = /(^|[^\/])(www\.[\S]+(\b|$))/gim;

        // Email addresses
        var emailAddressPattern = /[\w.]+@[a-zA-Z_-]+?(?:\.[a-zA-Z]{2,6})+/gim;

        return this
            .replace(urlPattern, '<a class="display" href="$&">$&</a>')
            .replace(pseudoUrlPattern, '$1<a class="display" href="http://$2">$2</a>')
            .replace(emailAddressPattern, '<a class="display" href="mailto:$&">$&</a>');
    };
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name, default_value) {
    var ca = document.cookie.split(';');
    for (var i=0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(name + '=') == 0) return c.substring(name.length + 1, c.length);
    }
    if (default_value == undefined) return null;
    return default_value;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

Array.prototype.shuffle = function() {
    var i = this.length, j, tempi, tempj;
    if ( i == 0 ) return false;
    while ( --i ) {
        j       = Math.floor( Math.random() * ( i + 1 ) );
        tempi   = this[i];
        tempj   = this[j];
        this[i] = tempj;
        this[j] = tempi;
    }
    return this;
}

function game_started() {
    console.log("Event: Game started");
    return;
}
