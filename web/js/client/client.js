
var client = null;

$(document).ready(init_client);

function init_client() {
    setTheme();
    $('body.preload').removeClass('preload');
    $('input#input, input#password').keydown(input_keydown);
    $('div.display').on('click', 'a.direction', go);
    $(document).mouseup(select_input);
    $(document).keydown(document_keypress);
    $('a#nav-menu').click(menu);
    $('a#nav-style').click(style);
    $('a.menuitem').click(hidemenu);
    $('a#nav-help').click(help);
    $('a#nav-who').click(who);
    client = new Client();
    clearConsole();
    openConnection();
}

var finalize = function() {
    closeConnection();
};

var pageHidden = function() {
};

var document_keypress = function(e) {
    var e = window.event ? window.event : e
    if (e.keyCode == 75 && (e.metaKey || e.ctrlKey) && !e.altKey) {
        clearConsole();
        return false;
    }
};

var input_keydown = function(e) {
    var key = (e.keyCode ? e.keyCode : e.which);
    // var source = e.target || e.srcElement;
    // console.log($(source).attr('id') + ' received ' + key);

    if (key == 13) { // The key-code for Enter
        input(e.target);
        return false;
    } else if (key == 38) { // Up arrow
        previous_command(e.target);
        return false;
    } else if (key == 40) { // Down arrow
        next_command(e.target);
        return false;
    }
};

var resetConnection = function(event) {
    closeConnection();
    openConnection();
    if (event) {
        event.stopImmediatePropagation();
        event.preventDefault();
    }
    return false;
};

var openConnection = function() {
    if (client == null) return;
    client.open();
    client.socket.onmessage = function(event) {
        receive_message(event.data);
    };
    client.socket.onopen = function(event) {
        log('Connection established.');
        reset();
        client.send("\n");
        focus();
    };
    client.socket.onclose = function(event) {
        log('WebSocket closed.');
        _write('Connection closed by foreign host.');
    }
    focus();
};

var closeConnection = function() {
    if (client == null) return;
    client.close();
    clearConsole();
};

var clearConsole = function(e) {
    clear();
    focus();
};

var style = function(e) {
    var theme = 'console';
    if ($('link#theme').attr('href').match(/console\.css$/)) {
        theme = 'parchment';
    }
    setTheme(theme);
    event.stopImmediatePropagation();
    event.preventDefault();
    focus();
    _scroll();
    return false;
};

var setTheme = function(theme) {
    theme = (theme == undefined) ? readCookie('theme', 'console') : theme;
    if (iphone) $('link#theme').attr({href : 'css/client/' + theme + '.css'});
    else $('link#theme').attr({href : 'css/client/' + theme + '.css'});
    createCookie('theme', theme, 30);
};

var character = function(event) {
    event.stopImmediatePropagation();
    event.preventDefault();
    return false;
};

var menu = function(event) {
    $('div.menu').slideToggle();
    event.stopImmediatePropagation();
    event.preventDefault();
    return false;
};

var hidemenu = function(event) {
    $('div.menu').slideUp();
};

var who = function(event) {
    $('div.menu').slideUp();
    event.stopImmediatePropagation();
    event.preventDefault();
    return false;
};

var help = function(event) {
    $('div.menu').slideUp();
    event.stopImmediatePropagation();
    event.preventDefault();
    return false;
};
