var ansispan = function (str) {
  str = substituteColors(str, ansispan.foregroundColors, 'foreground');
  str = substituteColors(str, ansispan.backgroundColors, 'background');

  //
  // `\033[1m` enables bold font, `\033[22m` disables it
  //
  str = str.replace(/\033\[1m/g, '<b>').replace(/\033\[22m/g, '</b>');

  //
  // `\033[3m` enables italics font, `\033[23m` disables it
  //
  str = str.replace(/\033\[3m/g, '<i>').replace(/\033\[23m/g, '</i>');

  //
  // `\033[4m` enables underline font, `\033[24m` disables it
  //
  str = str.replace(/\033\[4m/g, '<span class="underline">').replace(/\033\[24m/g, '</span>');

  //
  // NOT SUPPORTED
  // `\033[5m` enables blinking font, `\033[25m` disables it
  //
  str = str.replace(/\033\[5m/g, '<span class="blink">').replace(/\033\[25m/g, '</span>');

  //
  // NOT SUPPORTED
  // `\033[6m` enables flashing font, `\033[26m` disables it
  //
  str = str.replace(/\033\[6m/g, '<span class="flash">').replace(/\033\[26m/g, '</span>');

  //
  // NOT SUPPORTED
  // `\033[7m` enables inverse font, `\033[27m` disables it
  //
  str = str.replace(/\033\[7m/g, '<span class="inverse">').replace(/\033\[27m/g, '</span>');

  //
  // `\033[8m` enables hidden font, `\033[28m` disables it
  //
  str = str.replace(/\033\[8m/g, '<span class="hidden">').replace(/\033\[28m/g, '</span>');

  //
  // `\033[9m` enables line-through font, `\033[29m` disables it
  //
  str = str.replace(/\033\[9m/g, '<span class="line-through">').replace(/\033\[29m/g, '</span>');

  //
  // `\033[51m` enables fixed-width font, `\033[56` disables it
  //
  str = str.replace(/\033\[51m/g, '<span class="monospace">').replace(/\033\[56m/g, '</span>');

  str = str.replace(/\033\[m/g, '</span>');
  str = str.replace(/\033\[39m/g, '</span>');
  str = str.replace(/\033\[49m/g, '</span>');
  return str;
};

var substituteColors = function(str, dictionary, styleKey) {
  styleKey = styleKey ? styleKey : 'foreground'
  Object.keys(dictionary).forEach(function (ansi) {
    var span = '<span class="' + dictionary[ansi] + '-' + styleKey + '">';

    //
    // `\033[Xm` == `\033[0;Xm` sets color to `X`.
    //

    str = str.replace(
      new RegExp('\033\\[' + ansi + 'm', 'g'),
      span
    ).replace(
      new RegExp('\033\\[0;' + ansi + 'm', 'g'),
      span
    );
  });
  return str;
};

ansispan.foregroundColors = {
  '30': 'black',
  '31': 'red',
  '32': 'green',
  '33': 'yellow',
  '34': 'dodgerblue',
  '35': 'magenta',
  '36': 'cyan',
  '37': 'white'
};

ansispan.backgroundColors = {
  '40': 'black',
  '41': 'red',
  '42': 'green',
  '43': 'yellow',
  '44': 'dodgerblue',
  '45': 'magenta',
  '46': 'cyan',
  '47': 'white'
};

if (typeof module == "object" && typeof window == "undefined") {
  module.exports = ansispan;
}
