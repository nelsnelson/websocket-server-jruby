# websocket-server-jruby

[![License](https://img.shields.io/badge/license-MIT--2.0-blue.svg?style=flat)][license]

This is a small websocket server for [JRuby].

It is based on the [Netty project].  Netty is written in java, but I wanted to write ruby.


## Quick-start

Follow these instructions to get a websocket server echo program running in your web browser.


### Container

You may run the websocket server in a container. Using [colima] for a container runtime is recommended.

```sh
colima start
docker-compose up --detach
open http://localhost:4000/client.html
docker-compose down
```


Building the image or running the container:

```sh
docker build --squash --tag websocket-server-jruby .
docker run --detach --publish 4000:4000 --name websocket-server-jruby websocket-server-jruby
```


## Manually

Run directly with the required dependencies installed.


## Install mise-en-place

The [mise] CLI tool used to manage multiple runtime versions.

See: https://mise.jdx.dev/getting-started.html

```sh
curl https://mise.jdx.dev/install.sh | sh
~/.local/bin/mise --version
mise 2024.x.x
```

Enable mise activation in future zsh sessions.

```sh
echo 'eval "$(~/.local/bin/mise activate zsh)"' >> ~/.zshrc
```


## Install required runtime software

Use mise to install the runtime software defined as requirements
in the .tool-versions file.

```sh
mise install
```


Install the project dependencies.

```sh
bundle install
```


## Run

The entrypoint for the web application service may now be invoked from a command line interface terminal shell.

```sh
bundle exec ./websocket.rb &
open http://localhost:4000/client.html
```


## Build the gem

To clean the project, run unit tests, build the gem file, and verify that the built artifact works, execute:

```sh
bundle exec rake
```


## Publish the gem

To publish the gem, execute:

```sh
bundle exec rake publish
```


## Project file tree

Here is a bird's-eye view of the project layout.

```sh
# date && tree -A -I "logs|vendor|tmp|Gemfile.lock"
Sun Jul 31 15:17:32 CDT 2022
.
├── Dockerfile
├── Gemfile
├── LICENSE
├── README.md
├── Rakefile
├── docker-compose.yaml
├── exe
│   └── websocket
├── lib
│   ├── logging.rb
│   ├── server
│   │   └── mime_types.rb
│   ├── websocket
│   │   ├── arguments_parser.rb
│   │   ├── channel_initializer.rb
│   │   ├── config.rb
│   │   ├── encoding.rb
│   │   ├── file_server_channel_progressive_future_listener.rb
│   │   ├── frame_handler.rb
│   │   ├── header_helpers.rb
│   │   ├── http_static_file_server_handler.rb
│   │   ├── http_static_file_server_handler_instance_methods.rb
│   │   ├── idle_handler.rb
│   │   ├── idle_state_user_event_handler.rb
│   │   ├── instance_methods.rb
│   │   ├── listenable.rb
│   │   ├── message_handler.rb
│   │   ├── response_helpers.rb
│   │   ├── server.rb
│   │   ├── shutdown_hook.rb
│   │   ├── ssl_cipher_inspector.rb
│   │   ├── ssl_context_initialization.rb
│   │   ├── telnet_proxy.rb
│   │   ├── validation_helpers.rb
│   │   └── version.rb
│   ├── websocket-server.rb
│   ├── websocket_client.rb
│   └── websocket_server.rb
├── spec
│   ├── spec_helper.rb
│   ├── test_spec.rb
│   └── verify
│       └── verify_spec.rb
├── web
│   ├── client.html
│   ├── css
│   │   └── client
│   │       ├── console.css
│   │       └── parchment.css
│   ├── favicon.ico
│   ├── fonts
│   │   └── droidsansmono.v4.woff
│   └── js
│       ├── client
│       │   ├── ansispan.js
│       │   ├── client.js
│       │   ├── console.js
│       │   └── websocket.js
│       └── jquery.min.js
├── websocket-server-1.0.1-java.gem
├── websocket-server-jruby.gemspec
├── websocket.rb
└── websocket_server.png

12 directories, 51 files
```

[license]: https://gitlab.com/nelsnelson/websocket-server-jruby/blob/master/LICENSE
[mise]: https://mise.jdx.dev/
[colima]: https://github.com/abiosoft/colima
[Netty project]: https://github.com/netty/netty
[Java JDK]: https://www.java.com/en/download/
[JRuby]: https://jruby.org/download
