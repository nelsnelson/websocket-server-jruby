FROM jruby:9.4.6.0

WORKDIR /usr/src/websocket-server-jruby

COPY . .
RUN bundle install \
  && \
  rm Gemfile.lock


CMD ["./websocket.rb"]
